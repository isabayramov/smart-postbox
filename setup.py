from setuptools import setup, find_packages

with open(r'requirements.txt') as f:
    requirements = f.readlines()
    requirements = [req.strip() for req in requirements]

setup(
    name="Intelligent Postbox",
    version="0.0.1",
    author="Öxle Kilian, Bayramov Isa",
    author_email="ulwjr@studium.kit.edu, useec@studium.kit.edu",
    description="Package for Lecture Project 'Intelligent Postbox' ",
    long_description=' ',
    long_description_content_type="text/markdown",
    # url="https://github.com/pypa/sampleproject",
    # project_urls={
    #     "Bug Tracker": "https://github.com/pypa/sampleproject/issues",
    # },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    packages=find_packages(where="src"),
    python_requires=">=3.6",
    install_requires=requirements,
)