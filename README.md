## Installation
1) Clone the project and go to project root
2) Create a new environment and activate it
3) Follow instruction below for your Operating System

#### For linux:
1)  ```bash
    pip install -e .
    ```
2)  ```bash
    sudo apt-get install tesseract-ocr-deu
    ```

#### For Windows:
1)  ```bash
    pip install -e .
    ```
2)  download [tesseract-ocr for windows](https://tesseract-ocr.github.io/tessdoc/Downloads.html) and install it

3) go to `src.OCR.ocr` and change the path in `def configure_tesseract()` to your `tesseract-ocr` path

## How to use
1) go to project root and run 
    ```bash
        python src/app.py
    ```
2) On cmd output you will see the **adress** where your flask app is running. Go to these **adress** for first time to initialize Intelligent Postbox. Multiple call on home page of app does not initialize Postbox multiple time.
3) To check if there is a package in Postbox got to **adress/receiver**
4) To drop the package go to **adress/drop**
