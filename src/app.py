import os
import pandas as pd
from flask import Flask, jsonify
from yoda import YODA
from utils import get_src_dir, read_config


# get configurations
cfg = read_config()

# initialize process manager
master_yoda = YODA(config=cfg)

# initialize application
app = Flask(__name__)
app.secret_key = "@TODO CHANGE KEY"
BOX_INITIALIZED = False

# need to call for first time to start Post Box
# multiple call DOES NOT affect the process!
@app.route('/')
def home():
    global BOX_INITIALIZED 
    if not BOX_INITIALIZED:
        BOX_INITIALIZED = True
        master_yoda.loop()
    else:
        return 'Box is already initialized'
    
    
@app.route("/receiver")
def get_one_receiver():
    
    """
    This Method checks if there is a Paket for someone in Postbox. 
    
    input: None
    
    output: Json Object. Possible outputs: 
    if there is a paket --> {'message':'succes', 'status':200, 'receiver': {'name':'Max Mustermann', 'paket_id':1}}
    if there isn't any paket there --> {'message':'succes', 'status':200, 'receiver': None}
    if something went wrong in our side --> {'message':'failed', 'status':500, 'receiver': None}
    
    """
    
    receiver = None
    
    try:
        src_dir = get_src_dir()    
        csv_target = os.path.join(src_dir, 'post_stack.csv')
        receivers = pd.read_csv(csv_target)
    except:
        status = 500
        message = 'failed'       
    else:
        status = 200
        message = 'succes'
        
        if len(receivers) > 1: # check if there is a receiver
            
            # take max one person at a time
            receiver = receivers.to_dict('records')[1]        
    
    return jsonify(
        message=message,
        status=status,
        receiver=receiver, 
        )


@app.route("/drop")
def check_forwarding_to_bot():
    
    """
    This Method drops the Paket for TurtleBot and gives Response wenn process is finished. It works accrding FIFO principle.
    
    input: None
    
    output: Json Object. Possible outputs: 
    paket dropped succesfully and pocess finished --> {'message':'succes', 'status':200}
    if something went wrong in our side --> {'message':'failed', 'status':500}
    
    """
    
    try:             
        master_yoda.drop_package()            
    except:
        status = 500
        message = 'failed'
    else:
        status = 200
        message = 'succes'    
        
    return jsonify(
        message=message,
        status=status,
        )

if __name__ == '__main__':
    app.run(host='0.0.0.0')