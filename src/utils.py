import os
import time
import json
import yaml
import pandas as pd


def get_src_dir():
    cwd = os.getcwd()   
    l = cwd.split('src')[:-1]
    
    return os.path.join(*l, 'src')

def write_json(data: dict(), target):
    with open(target, 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)
        
def read_json(target):
    with open(target, encoding='utf-8') as f:
        data = json.load(f)
    return data

def read_config():
    with open('configuration.yml', 'r') as configuration:
        conf = yaml.safe_load(configuration)
    return conf

def update_post_stack( name: str=None, add: bool=True, init: bool=False):
    
    src_dir = get_src_dir()    
    csv_target = os.path.join(src_dir, 'post_stack.csv')
    
    file_exist = os.path.isfile(csv_target)
    
    if file_exist:
        post_stack = pd.read_csv(csv_target)
    else:
        post_stack = pd.DataFrame({'name':['default'], 'paket_id':[0]})
        
    if add:
        paket_id = str(time.time())        
        new_post = pd.DataFrame({'name':name, 'paket_id':paket_id}, index=[len(post_stack)])
        
        post_stack = pd.concat([post_stack, new_post])

    elif init:
        pass
    else:
        try:
            post_stack = post_stack.drop([1])
        except:
            pass

    # save updated csv
    post_stack.to_csv(csv_target, index=False)

    # number of packages to be delivered
    return len(post_stack) - 1