from distutils import dist
from urllib.request import urlopen
import json
from Postprocessing.similarity_calculators import SimilarityCalculater
from utils import write_json, read_json


def get_employees(save_load_target: str):
    REQUEST_URL = 'https://www.aifb.kit.edu/web/Spezial:Semantische_Suche/format%3Djson/link%3Dall/headers%3Dshow/searchlabel%3DJSON/class%3Dsortable-20wikitable-20smwtable/mainlabel%3D/sort%3D/order%3Dasc/offset%3D0/limit%3D100/-5B-5BKategorie:Mitarbeiter-5D-5D-20-5B-5BRaum::~1A-2D*-5D-5D/-3FAkademischer-20Titel/-3FRaum/-3FBild/prettyprint%3Dtrue/unescape%3Dtrue'
    
    try:
        # get emloyees
        json_url = urlopen(REQUEST_URL)    
        data = json.loads(json_url.read())
        employees = list(data['results'].keys())
        
        # save it local
        write_json(data=employees, target=save_load_target)
        
    except:
        # get employees from local if request failed
        employees = read_json(save_load_target)        
    
    return employees

def delete_fix(raw_ocr: str, similarity_metric='Levenshtein'):
    
    """
    This method removes all words, which are known beforehand.
    """
    
    simcalc = SimilarityCalculater()
    
    raw_ocr = raw_ocr.strip()
    ocred = [line.strip() for line in raw_ocr.split('\n')]
    
    if len(ocred) > 2:
        # delete fix strings
        fix_strings = ['institut aifb', 'Kollegiengebäude am Kronenplatz', 'Kaiserstraße', 'Karlsruhe']
        for fix_string in fix_strings:

            candidate = False
            distance = 0
            # total_distance = 0.0  # sum of all distances. needed to calculate probability of true matching

            # find fix strings in text and delete
            for i in ocred:
                if i == '':
                    ocred.remove(i)
                else:
                    similarity = simcalc.calculate_distance(algorithm=similarity_metric, word1=fix_string, word2=i)
                    # total_distance += similarity

                    if similarity>distance:
                        candidate = i
                        distance = similarity

            # delete, if the probability of true matching is higher than 50%
            if distance != 0:       
                # if distance/total_distance >= 0.5:
                if distance >= 0.5:
                    ocred.remove(candidate)
            
    return ocred

def name_matching(ocred_text: list(), save_load_target='persons.json', similarity_metric='Levenshtein'):
    
    """
    This method assumes that receiver name and surname are in same line.
    It finds the person with high probability of being the receiver.
    """
    
    simcalc = SimilarityCalculater()
    employees = get_employees(save_load_target)
    
    
    similarity = 0.0
    total_similarity = 0.0  # sum of all distances. needed to calculate probability of true matching
    receiver_name = None
    
    # for each candidate person calculate the similarity and find person with highest similarity score 
    for name in employees:
        d = [simcalc.calculate_distance(algorithm=similarity_metric, word1=i, word2=name) for i in ocred_text]
        d = max(d)
        total_similarity += d
        
        if d >= similarity:
            similarity = d
            receiver_name = name
    
    if similarity < 0.3:
        receiver_name = 'secretary'
        similarity = 1.0
        
    return {'name':receiver_name, 'similarity':similarity}
