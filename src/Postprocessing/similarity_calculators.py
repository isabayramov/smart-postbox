import textdistance


class SimilarityCalculater():
    
    def __init__(self):
        
        """"
        This Class creates instance for different distance calculation Methods and performs distance calculation for 2 given strings.
        
        input:
        ------
        :algorithm: Name of Distance Calculation Algorithm to use
        
        output:
        ------
        :similarity: Similarity score (between 1 and 0) of 2 strings. 1 Means exactly same and 0 means total different. 
        
        """
        
        self.algorithms= dict()
        
        self.algorithms['Levenshtein'] = textdistance.Levenshtein(external=False)
        # self.algorithms['Hamming'] = textdistance.Hamming(external=False)
        # self.algorithms['Mlipns'] = textdistance.MLIPNS(external=False)
        # self.algorithms['DamerauLevenshtein'] = textdistance.DamerauLevenshtein(external=False)
        # self.algorithms['JaroWinkler'] = textdistance.JaroWinkler(external=False)
        # self.algorithms['StrCmp95'] = textdistance.StrCmp95(external=False)
        # self.algorithms['NeedlemanWunsch'] = textdistance.NeedlemanWunsch(external=False)
        # self.algorithms['Gotoh'] = textdistance.Gotoh(external=False)
        # self.algorithms['SmithWaterman'] = textdistance.SmithWaterman(external=False)
        
    def calculate_distance(self, algorithm: str, word1: str, word2: str):
        """
        For More Information read hier:https://github.com/life4/textdistance

        """
        algo = self.algorithms[algorithm]

        similarity = algo.normalized_similarity(word1, word2)

        return similarity
    