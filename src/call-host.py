from utils import read_config
import requests


endpoint = read_config()['app']['endpoint']
total_attempts = 5
attempt = 0

while attempt<total_attempts:
	try:
		resp = requests.get(endpoint, timeout=5)
		break
	except:
		attempt += 1