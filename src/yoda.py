import os 
import time

from sensors import MotionDetector, DistanceDetector, Board_PCA9685, Lights, take_image
from OCR.ocr import configure_tesseract, do_ocr
from Postprocessing.find_recipient import delete_fix, name_matching
from utils import get_src_dir, update_post_stack


class YODA():
    ACTIVE = False  # used to accept and process only one package at a time 

    def __init__(self, config: dict):
        
        # initialize config
        self.config = config

        # initialize box
        self.BOX_SHELFS = self.config['shelf']['BOX_SHELFS']
        self.EMPTY_SHELFS = self.BOX_SHELFS
        self.DELAY_FOR_SHELF_CHANGE = self.config['shelf']['DELAY_FOR_SHELF_CHANGE']
        self.lights_delay = self.config['lights']['delay']

        # initialize sesors
        self.distance_detector = DistanceDetector(**self.config['distance_detector'])
        self.board = Board_PCA9685(**self.config['board'])     
        self.lights = Lights(**self.config['lights'])   
        self.motion_detector = MotionDetector(on_motion=self.at_package_income, **self.config['motion_detector'])
        
        # create intial post_stack.csv file for storing receivers
        package_in_box = update_post_stack(add=False)
        self.EMPTY_SHELFS -=  package_in_box

    def loop(self):
        while True:
            if YODA.ACTIVE:
                pass
            else:
                YODA.ACTIVE = True
                # self.motion_detector.start_detector()
                if self.motion_detector.detector.motion_detected:
                    self.at_package_income()

                # can accept new packages again
                YODA.ACTIVE = False
                time.sleep(1)

    def identify_and_write_receiver(self, img_path: str):

        src_dir = get_src_dir()

        # get ocr result and delete known words
        text = do_ocr(img_pth = img_path)   
        simplified_text = delete_fix(raw_ocr=text)

        # find receiver
        target = os.path.join(src_dir, 'persons.json')
        match_result = name_matching(ocred_text=simplified_text, save_load_target=target, similarity_metric='Levenshtein')

        # write new receiver in csv
        total_packages_to_deliver = update_post_stack(add=True, name=match_result['name'])

        return total_packages_to_deliver

    def change_package_shelf(self, current_shelf):

        gate_channels = self.board.servo_channels[current_shelf-1]

        # open gate
        self.board.move_servos(target='max', servo_channels=gate_channels)
        time.sleep(self.DELAY_FOR_SHELF_CHANGE)

        # close gate
        self.board.move_servos(target='min', servo_channels=gate_channels)
        time.sleep(0.5)

    def at_package_income(self):

        """
        Define Function to call whenn a Motion is detected.

        input: None

        output: 

        """
        
        # start process if there is some empty shelf in Box else light 
        if self.EMPTY_SHELFS:
            
            # light yellow to say that the process started
            self.lights.on('yellow')

            # use distance to check if a package is in box
            current_distance = self.distance_detector.get_distance()

            # take image of package                                                           
            take_image_response = take_image(**self.config['camera'])
            img_pth = str(take_image_response['img_pth'])
            time.sleep(1.0)

            # turn off yelllow and turn on green to say that package can be put it
            self.lights.off('yellow')
            self.lights.on('green')

            # get adress on the package and save if package is in box
            try_if_in = 0
            while try_if_in < 2:
                time.sleep(self.DELAY_FOR_SHELF_CHANGE)
                delta_distance = 1 - self.distance_detector.get_distance() / current_distance
                if delta_distance >= 0.2:
                    self.identify_and_write_receiver(img_path=img_pth)

                    # move package to nxt shelf if is empty (shelf at top has high id and at bottom low id)
                    for i in range(self.EMPTY_SHELFS-1):
                        current_shelf = self.BOX_SHELFS - i
                        self.change_package_shelf(current_shelf=current_shelf)
                        current_shelf -= 1

                    self.EMPTY_SHELFS -= 1
                    break
                else:
                    try_if_in += 1
            
            # turn off green to say that, no package could be put in after that
            self.lights.off('green')
        else:
            # light red to say that box is full
            self.lights.on('red', self.lights_delay)


    def drop_package(self):

        for shelf in range(1, self.BOX_SHELFS - self.EMPTY_SHELFS +1):
            self.change_package_shelf(shelf)     
        
        self.EMPTY_SHELFS += 1
