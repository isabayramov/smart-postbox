from multiprocessing.managers import DictProxy
from pathlib import Path
import subprocess
import time

import Adafruit_PCA9685
from gpiozero import MotionSensor, DistanceSensor, LED
import RPi.GPIO as GPIO


def take_image(save_to: str, **kwargs):
    
    """
    Uses raspistill module and takes-saves image in as jpg.
    
    input:
    save_to: Full Path to save image.
    
    output:
    key vaue pairs inscluding final status of process, message about the status and if succesfull, image path.
    
    """
    
    try:
        # create save directory if not exist
        save_to = Path(save_to)
        if save_to != '':
            save_to.mkdir(parents=True, exist_ok=True)
    except:
        return {'message': f'Not a Directory or Path! Your input: {save_to}', 'status':'400'}
    
    # save location
    at = 'image.jpg'
    save_to = Path(save_to, at)
    
    # take picture and save
    out = subprocess.run(f"raspistill -vf -hf -o {save_to}", shell=True, capture_output=True, text=True)
    if out.returncode != 0:
        return {'message': out, 'status':'500'}
    else:
        return {'message':'succes', 'status':'200', 'img_pth': save_to}

    
class MotionDetector():

    """
    For more details of used library go to 
    https://gpiozero.readthedocs.io/en/stable/api_input.html#motionsensor-d-sun-pir
    
    """

    def __init__(self, on_motion: callable, pin: int, sample_rate : float, **kwargs):
        self.detector = MotionSensor(pin, sample_rate =sample_rate )
        self.on_motion = on_motion
        print(" Motion Detector is ready! ", flush=True)

    def start_detector(self):
        """ Initializes detection process with on motion and on_no_motion todo functions. """
        if self.detector.motion_detected:
            self.on_motion()      

    def on_no_motion(self):
        
        """
        Define Function to call whenn there is no motion detected.
        Currently we just pass when there is no motion.
        """
        pass

    
class DistanceDetector():

    """
    Code Source:
    https://tutorials-raspberrypi.de/entfernung-messen-mit-ultraschallsensor-hc-sr04/#:~:text=Entfernung%20messen%20mit%20Ultraschallsensor%20HC%2DSR04%20%E2%80%93%20Raspberry%20Pi,-Facebook%20Twitter%20LinkedIn&text=Bei%20vielen%20(Au%C3%9Fen%2D)Projekten,und%20sind%20dabei%20erstaunlich%20genau.
    
    """

    def __init__(self, pin_echo: int, pin_trigger : int, **kwargs):

        # initialize pins
        self.pin_echo = pin_echo
        self.pin_trigger = pin_trigger 

        #GPIO Modus (BOARD / BCM)
        GPIO.setmode(GPIO.BCM)

        #Richtung der GPIO-Pins festlegen (IN / OUT)
        GPIO.setup(pin_trigger, GPIO.OUT)
        GPIO.setup(pin_echo, GPIO.IN)

        # self.distance_detector = DistanceSensor(echo=pin_echo, trigger=pin_trigger)
        print(" Distance Sensor is ready! ", flush=True)

    def get_distance(self):
        self.distance()
        time.sleep(0.05)
        return self.distance()

    def distance(self):
        # setze Trigger auf HIGH
        GPIO.output(self.pin_trigger, True)
    
        # setze Trigger nach 0.01ms aus LOW
        time.sleep(0.00001)
        GPIO.output(self.pin_trigger, False)
    
        StartZeit = time.time()
        StopZeit = time.time()
    
        # speichere Startzeit
        while GPIO.input(self.pin_echo) == 0:
            StartZeit = time.time()
    
        # speichere Ankunftszeit
        while GPIO.input(self.pin_echo) == 1:
            StopZeit = time.time()
    
        # Zeit Differenz zwischen Start und Ankunft
        TimeElapsed = StopZeit - StartZeit
        # mit der Schallgeschwindigkeit (34300 cm/s) multiplizieren
        # und durch 2 teilen, da hin und zurueck
        distanz = (TimeElapsed * 34300) / 2
    
        return distanz

    
class Board_PCA9685():
    
    def __init__(self, servo_channels: list[list[int]] or dict[list[int]], i2c_bus_number: int, 
                 servo_min: int, servo_max: int, **kwargs):
        
        # initialize board
        self.pwm = Adafruit_PCA9685.PCA9685(busnum=i2c_bus_number)
         
        # Set frequency to 60hz, good for servos.
        self.pwm.set_pwm_freq(60)
        
        # initialize servo channels
        self.servo_channels = servo_channels        
        
        # Define desired servo pulse range out of 4096
        self.range = {'min':servo_min, 'max':servo_max}
        
    def move_servos(self, target: str, servo_channels: list[int]):        
        assert target in ['min', 'max']
        
        # set where to move and move all servos
        try:        
            target = self.range[target]        
            for channel in servo_channels:
                self.pwm.set_pwm(channel, 0, target)
            
            time.sleep(0.5)  # wait till motors moves to target 

            # stop motors to avoid shaking
            for channel in servo_channels:
                self.pwm.set_pwm(channel, 0, 0)

            message = 'succes'
            status = '200'
        
        except:
            message = 'failed'
            status = '500'
        
        return {'status':status, 'message': message}


class Lights():

    def __init__(self, leds: dict, **kwargs):   
        self.leds = self.initialize_lights(leds)

    def initialize_lights(self, leds):
        for led in leds:
            channel = leds[led]
            leds[led] = LED(channel)
        return leds

    def on(self, led_sensor: str, duration: float=None):
        self.leds[led_sensor].on()
        if duration:
            time.sleep(duration)
            self.off(led_sensor)

    def off(self, led_sensor: str):
        self.leds[led_sensor].off()
