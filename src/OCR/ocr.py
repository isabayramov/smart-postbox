import cv2 
import pytesseract
from OCR.image_preprocessing import process_image_for_ocr


def configure_tesseract():
    
    # change path to your tesseract
    # pytesseract.pytesseract.tesseract_cmd = r'.\Tesseract-OCR\tesseract.exe'
    
    # Adding custom options
    # Possible Options for use in Page Segmentation Modes(PSM)
    # OSD = Orientation and script detection.
    # 1    Automatic page segmentation with OSD.
    # 3    Fully automatic page segmentation, but no OSD. (Default)
    
    custom_config = r'--oem 1 --psm 1'
    
    return custom_config

def do_ocr(img_pth: str):

    img = process_image_for_ocr(img_pth)
    
    custom_config = configure_tesseract()
    result = pytesseract.image_to_string(img, config=custom_config, lang='deu')
    return result

